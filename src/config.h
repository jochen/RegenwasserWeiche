#include <Arduino.h>

#define DEFAULT_HOSTNAME "regenwasser"

#define CONFIG_CHAR_LENGTH 40

#define POWER_PIN D8
#define MQTT_CONNECTED D7

extern const char *version;
extern const char *buildtime;

struct hostConfig
{
  char hostName[CONFIG_CHAR_LENGTH];
  char realName[CONFIG_CHAR_LENGTH];
  unsigned long crc;
};

struct logicDebugPin {
  const char *name;
  uint8_t pin;
};

#define SERVO_PWM_PIN1 D4

//#define SENSORCOUNT 4

/* #ifndef _BUTTONSCONFIG_H
#define _BUTTONSCONFIG_H
buttonPinConfig buttonsConfig[] = {
    {4, 12},
    {5, 13}
    };
#endif
 */

#define MAXTOPICLENGTH 128
#define MAXPAYLOADLENGTH 30
#define MAXURLLENGHT 128

//#define LONGPRESSMILLIS 3000
struct appConfig
{
  char mqttTopicprefix[CONFIG_CHAR_LENGTH] = {};
  char mqttHost[CONFIG_CHAR_LENGTH] = {};
  unsigned int mqttPort = 0;
  unsigned int servoDegreePercent0 = 170;
  unsigned int servoDegreePercent100 = 50;
  unsigned int powerOnTimeoutmillis = 10000;
  //float R2_CAL[SENSORCOUNT] = {};
  unsigned long crc = 0;
};


