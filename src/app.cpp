#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>

#include <Servo.h>


#define DEBUGOUT // Comment to disable debug output

#define DBG_OUTPUT Serial

#ifdef DEBUGOUT
#define DEBUGLOG(...) DBG_OUTPUT.printf_P(__VA_ARGS__)
#else
#define DEBUGLOG(...)
#endif
#include "config.h"

extern appConfig myappConfig;
extern hostConfig myhostConfig;

//extern void sendUdpSyslog(String msgtosend);
extern void saveAppConfig();
extern void broadcastWS(String type, int value);
extern void sendDebugWS(String name, String type, String value);
extern uint16_t mqttPublish(String topic, String payload);

int last_mqtt_package_id = 0;
int last_publish_success_id = 0;

unsigned long apploopmillis = 0;
unsigned long powerOnLoopmillis = 0;

Servo servoObject;

//unsigned int servoDegreePercent0 = 170;
//unsigned int servoDegreePercent100 = 50;
//unsigned int powerOnTimeoutmillis = 10000;

unsigned int servoDegree = myappConfig.servoDegreePercent0;




int servo(int degree)
{
  servoDegree = degree;
  return degree;
}

int getPercent(int percent)
{
  int result = -1;
  if (myappConfig.servoDegreePercent100 > myappConfig.servoDegreePercent0){
    result = (100 * (servoDegree - myappConfig.servoDegreePercent0) / (myappConfig.servoDegreePercent100 - myappConfig.servoDegreePercent0));
  }
  if (myappConfig.servoDegreePercent100 < myappConfig.servoDegreePercent0){
    result = 100 - (100 * (servoDegree - myappConfig.servoDegreePercent100) / (myappConfig.servoDegreePercent0 - myappConfig.servoDegreePercent100)); 
  }
  return result;
}

int setPercent(int percent)
{
  digitalWrite(POWER_PIN, HIGH);
  powerOnLoopmillis = millis();
  if (myappConfig.servoDegreePercent100 > myappConfig.servoDegreePercent0){
    servoDegree = (((myappConfig.servoDegreePercent100 - myappConfig.servoDegreePercent0) * 
    percent) / 100) + myappConfig.servoDegreePercent0;
  }
  if (myappConfig.servoDegreePercent100 < myappConfig.servoDegreePercent0){
    servoDegree = (((myappConfig.servoDegreePercent0 - myappConfig.servoDegreePercent100) * 
    (100 - percent)) / 100) + myappConfig.servoDegreePercent100;
  }
  Serial.print(F("setPercent: "));
  Serial.print(percent);
  Serial.print(" degree: ");
  Serial.print(servoDegree);
  Serial.println();
  mqttPublish(String(myappConfig.mqttTopicprefix) + "/percent", String(percent));
  broadcastWS("percent", percent);
  return percent;
}

void setupApp()
{
  pinMode(POWER_PIN, OUTPUT);
  digitalWrite(POWER_PIN, LOW);
  servoObject.attach(SERVO_PWM_PIN1);

}

void loopApp()
{
  servoObject.write(servoDegree);

  apploopmillis = millis();
  if (powerOnLoopmillis != 0 && (powerOnLoopmillis + myappConfig.powerOnTimeoutmillis) < apploopmillis){
      powerOnLoopmillis = 0;
      digitalWrite(POWER_PIN, LOW);
  }
}