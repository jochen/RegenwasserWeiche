#include <Arduino.h>
#include "ArduinoJson.h"
#include <limits.h>
#include "FS.h"
#include "ESPAsyncTCP.h"
#include "ESPAsyncWebServer.h"

#define DEBUGOUT // Comment to enable debug output

#define DBG_OUTPUT Serial

#ifdef DEBUGOUT
#define DEBUGLOG(...) DBG_OUTPUT.printf(__VA_ARGS__)
#else
#define DEBUGLOG(...)
#endif

extern void addFileToWWW(String path);
extern void delFileFromWWW(String path);


void handleFileUpload(AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final)
{
    static File fsUploadFile;
    static size_t fileSize = 0;

    if (!index)
    { // Start
        DEBUGLOG("handleFileUpload Name: %s\r\n", filename.c_str());
        if (!filename.startsWith("/"))
            filename = "/" + filename;
        fsUploadFile = SPIFFS.open(filename, "w");
        DEBUGLOG("First upload part.\r\n");
    }
    // Continue
    if (fsUploadFile)
    {
        DEBUGLOG("Continue upload part. Size = %u\r\n", len);
        if (fsUploadFile.write(data, len) != len)
        {
            DEBUGLOG("Write error during upload\n");
        }
        else
            fileSize += len;
    }
    /*for (size_t i = 0; i < len; i++) {
	if (fsUploadFile)
	fsUploadFile.write(data[i]);
	}*/
    if (final)
    { // End
        if (fsUploadFile)
        {
            fsUploadFile.close();
        }
        DEBUGLOG("handleFileUpload Size: %u\n", fileSize);
        fileSize = 0;
        if (filename.startsWith("/w/"))
        {
            addFileToWWW(filename);
        }
    }
}

void handleFileDelete(AsyncWebServerRequest *request)
{
    //if (!checkAuth(request))
    //	return request->requestAuthentication();
    if (request->args() == 0)
        return request->send(500, "text/plain", "BAD ARGS");
    String path = request->arg(0U);
    DEBUGLOG("handleFileDelete: %s\r\n", path.c_str());
    if (path == "/")
        return request->send(500, "text/plain", "BAD PATH");
    if (!SPIFFS.exists(path))
        return request->send(404, "text/plain", "FileNotFound");
    SPIFFS.remove(path);
    request->send(200, "text/plain", "");
    if (path.startsWith("/w/"))
    {
        delFileFromWWW(path);
    }
    path = String(); // Remove? Useless statement?
}

void updateFirmware(AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final)
{
    if (!index)
    {
        DEBUGLOG("Update Start: %s\n", filename.c_str());
        Update.runAsync(true);
        if (!Update.begin((ESP.getFreeSketchSpace() - 0x1000) & 0xFFFFF000))
        {
            Update.printError(DBG_OUTPUT);
        }
    }
    if (!Update.hasError())
    {
        if (Update.write(data, len) != len)
        {
            Update.printError(DBG_OUTPUT);
        }
    }
    if (final)
    {
        if (Update.end(true))
        {
            DEBUGLOG("Update Success: %uB\n", index + len);
        }
        else
        {
            Update.printError(DBG_OUTPUT);
        }
    }
}

void handleFileList(AsyncWebServerRequest *request)
{
    String path;
    if (request->hasArg("dir"))
    {
        path = request->arg("dir");
    }
    else
    {
        path = String("/");
    }
    DEBUGLOG("handleFileList: %s\r\n", path.c_str());
    Dir dir = SPIFFS.openDir(path);
    path = String();

    String output = "[";
    while (dir.next())
    {
        File entry = dir.openFile("r");
        if (true) //entry.name()!="secret.json") // Do not show secrets
        {
            if (output != "[")
                output += ',';
            output += "{\"name\":\"";
            output += String(entry.name());
            output += "\"}";
        }
        entry.close();
    }

    output += "]";
    DEBUGLOG("%s\r\n", output.c_str());
    request->send(200, "text/json", output);
}

String getContentType(String filename)
{
    if (filename.endsWith(".htm"))
        return F("text/html");
    else if (filename.endsWith(".html"))
        return F("text/html");
    else if (filename.endsWith(".css"))
        return F("text/css");
    else if (filename.endsWith(".json"))
        return F("application/json");
    else if (filename.endsWith(".js"))
        return F("application/javascript");
    else if (filename.endsWith(".png"))
        return F("image/png");
    else if (filename.endsWith(".gif"))
        return F("image/gif");
    else if (filename.endsWith(".jpg"))
        return F("image/jpeg");
    else if (filename.endsWith(".ico"))
        return F("image/x-icon");
    else if (filename.endsWith(".xml"))
        return F("text/xml");
    else if (filename.endsWith(".pdf"))
        return F("application/x-pdf");
    else if (filename.endsWith(".zip"))
        return F("application/x-zip");
    else if (filename.endsWith(".otf"))
        return F("application/x-font-opentype");
    else if (filename.endsWith(".eot"))
        return F("application/vnd.ms-fontobject");
    else if (filename.endsWith(".svg"))
        return F("image/svg+xml");
    else if (filename.endsWith(".woff"))
        return F("application/x-font-woff");
    else if (filename.endsWith(".woff2"))
        return F("application/x-font-woff2");
    else if (filename.endsWith(".ttf"))
        return F("application/x-font-ttf");
    else if (filename.endsWith(".gz")) //gziped version of files exist, so send the coreesponding filetype for a .gz file-ending (i.e. of a font or css)
    {
        if (filename.indexOf(".htm.") >= 0)
        {
            return F("text/html");
        }
        else if (filename.indexOf(".html.") >= 0)
        {
            return F("text/html");
        }
        else if (filename.indexOf(".css.") >= 0)
        {
            return F("text/css");
        }
        else if (filename.indexOf(".js.") >= 0)
        {
            return F("application/javascript");
        }
        else if (filename.indexOf(".json.") >= 0)
        {
            return F("application/json");
        }
        else if (filename.indexOf(".png.") >= 0)
        {
            return F("image/png");
        }
        else if (filename.indexOf(".gif.") >= 0)
        {
            return F("image/gif");
        }
        else if (filename.indexOf(".jpg.") >= 0)
        {
            return F("image/jpeg");
        }
        else if (filename.indexOf(".ico.") >= 0)
        {
            return F("image/x-icon");
        }
        else if (filename.indexOf(".xml.") >= 0)
        {
            return F("text/xml");
        }
        else if (filename.indexOf(".pdf.") >= 0)
        {
            return F("application/x-pdf");
        }
        else if (filename.indexOf(".zip.") >= 0)
        {
            return F("application/x-zip");
        }
        else if (filename.indexOf(".otf") >= 0)
        {
            return F("application/x-font-opentype");
        }
        else if (filename.indexOf(".eot") >= 0)
        {
            return F("application/vnd.ms-fontobject");
        }
        else if (filename.indexOf(".svg") >= 0)
        {
            return F("image/svg+xml");
        }
        else if (filename.indexOf(".woff") >= 0)
        {
            return F("application/x-font-woff");
        }
        else if (filename.indexOf(".woff2") >= 0)
        {
            return F("application/x-font-woff2");
        }
        else if (filename.indexOf(".ttf") >= 0)
        {
            return F("application/x-font-ttf");
        }

        return "application/x-gzip";

    } //end-else-normal-gzip

    return "text/plain";
}

bool handleFileRead(AsyncWebServerRequest *request)
{
    String path = request->url();
    DEBUGLOG("path:%s\r\n", path.c_str());
    if (!path.startsWith("/w"))
        path = "/w" + path;

    if (path.endsWith("/"))
        path += "index.html";
#define Debug(...)
#define DebugF Debug
#define Debugln Debug
#define DebuglnF Debug
    String contentType = getContentType(path);
    DEBUGLOG("contentType:%s\r\n", contentType.c_str());

    String pathWithGz = path + ".gz";

    DebugF("handleFileRead ");
    Debug(path);

    DEBUGLOG("path:%s\r\n", path.c_str());

    if (SPIFFS.exists(pathWithGz) || SPIFFS.exists(path))
    {
        if (SPIFFS.exists(pathWithGz))
        {
            path += ".gz";
            DebugF(".gz");
        }

        DebuglnF(" found on FS");
        DebugF("ContentType: ");
        Debugln(contentType);

        AsyncWebServerResponse *response = request->beginResponse(SPIFFS, path, contentType);
        if (path.endsWith(".gz"))
        {
            DebuglnF("Add header content-encoding: gzip");
            response->addHeader("Content-Encoding", "gzip");
        }
        request->send(response);
        return true;
    }

    Debugln("");

    request->send(404, "text/plain", "File Not Found");
    return false;
}
