#!/bin/bash

test -d spiffs || mkdir spiffs

rm -v spiffs/*

cp -v -t spiffs/ build/index.html build/manifest.json build/favicon.ico

for ext in js css; do
  echo "File extention: $ext"
  number=1
  (cd build; find static -name "*.${ext}") | while read datei; do
  
    echo "$datei"
    
    grep -q "$datei" spiffs/index.html || continue
    
    cp -v "build/$datei" "spiffs/${ext:0:1}${number}.${ext}"
    gzip -v "spiffs/${ext:0:1}${number}.${ext}"
    
    sed -i -e "s#${datei}#${ext:0:1}${number}.${ext}.gz#" spiffs/index.html
    
    number=$[ $number+1 ]
  
  done
done

