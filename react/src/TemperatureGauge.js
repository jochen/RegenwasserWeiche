import React from 'react';
//import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import calcColor from './calcColor';
import Gauge from 'react-svg-gauge';

const styles = {
    root: {
        //display: 'flex',
        height: 300,
    },
    slider: {
        padding: '0px 22px',
    },
};


class TemperatureGauge extends React.Component {
    constructor(props) {
        super(props);
        this.wsHandler = this.wsHandler.bind(this);
        this.state = {
            value: 90,
            min: 15,
            max: 90,
            color: new calcColor("#00c4ff", "#ff0400", 15, 90),
        };
        //console.log(this.state.color)

    }


    wsHandler(ev) {
        //console.log(ev);
        try {
            switch (ev.type) {
                case "open":
                    //console.log(decodeddata.value);
                    this.props.socket.send(JSON.stringify({
                        type: "temperature",
                        value: this.state.value,
                    }))
                    this.props.socket.send(JSON.stringify({
                        type: "maxtemperature",
                        max: this.state.max,
                    }))
                    break;

                case "message":
                    console.log(this.constructor.name, ev);
                    const decodeddata = JSON.parse(ev.data);
                    switch (decodeddata.type) {
                        case "temperature":
                            console.log(decodeddata);
                            if (decodeddata.result) {
                                this.setState({
                                    value: decodeddata.result / 10
                                });
                            }
                            if (decodeddata.value) {
                                this.setState({
                                    value: decodeddata.value / 10
                                });
                            }
                            break;
                        case "maxtemperature":
                            console.log(decodeddata);
                            if (decodeddata.result) {
                                this.setState({
                                    max: decodeddata.result
                                });
                            }
                            if (decodeddata.value) {
                                this.setState({
                                    max: decodeddata.value
                                });
                            }
                            this.state.color.max = this.state.max;
                            break;
                        default:
                            break;

                    }
                    break;
                default:
                    break;

            }
        } catch (e) {
            console.log(e, ev.data);
        }
    }
    componentDidMount() {
        this.props.socket.addEventListener("message", this.wsHandler);
        this.props.socket.addEventListener("open", this.wsHandler);
    }

    componentWillUnmount() {
        clearTimeout(this.timer);
        this.props.socket.removeEventListener("message", this.wsHandler);
        this.props.socket.removeEventListener("open", this.wsHandler);
    }

    render() {
        const { classes } = this.props;
        const { label } = this.props;
        const { value } = this.state;
        const { color } = this.state;
        const { min } = this.state;
        const { max } = this.state;

        return (
            <div>
                <Gauge
                    value={value}
                    width={150} height={130}
                    min={min} max={max}
                    label={label}
                    color={color.getColor(value)}
                    valueLabelStyle={{
                        fontSize:25,
                        fill:color.getColor(value)
                        }}
                />
            </div>
        );
    }
}

TemperatureGauge.propTypes = {
    classes: PropTypes.object.isRequired,
    socket: PropTypes.object.isRequired,
};

export default withStyles(styles)(TemperatureGauge);