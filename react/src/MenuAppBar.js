import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import OnlineIndicator from './OnlineIndicator';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { HashLink as Link } from 'react-router-hash-link';
import MoreVertIcon from '@material-ui/icons/MoreVert';

const styles = {
  root: {
    width: "100%",
    
    //minHeight: '100vh',
    //flexGrow: 1,
    //"text-align": center;
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

class MenuAppBar extends Component {
  constructor(props) {
    super(props);
    this.wsHandler = this.wsHandler.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.state = {
      anchorEl: null,
      routeName: null,
      realName: this.props.realName,
    };
  }
  componentDidMount() {
    this.props.socket.addEventListener("message", this.wsHandler);
  }

  componentWillUnmount() {
    this.props.socket.removeEventListener("message", this.wsHandler);
  }

  wsHandler(ev) {
    try {
      switch (ev.type) {
        case "message":
          console.log(this.constructor.name, ev);
          //console.log(ev.data);
          const decodeddata = JSON.parse(ev.data);
          switch (decodeddata.type) {
            case "info":
              //console.log(decodeddata.value);
              this.setState({
                realName: decodeddata.realname,
              })
              break;              
            default:
              break;
          }
          break;
        default:
          break;

      }
    } catch (e) {
      console.log(e, ev.data);
    }

  }


  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
    //this.setState({ routeName: "foo"});
    //console.log("click", event.nativeEvent.target.outerText);
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { classes, location } = this.props;
    const { auth, anchorEl } = this.state;
    const open = Boolean(anchorEl);

    return (
      <div className={classes.root}>
        <AppBar position="static"
          title={this.state.realName}
        >
          <Toolbar>
            <div>
            <IconButton className={classes.menuButton} color="inherit" aria-label="Menu"           
                aria-owns={anchorEl ? 'simple-menu' : undefined}
                aria-haspopup="true"
                onClick={this.handleClick}
              >
              <MenuIcon />
                </IconButton>
              <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={this.handleClose}
              >
                <MenuItem onClick={this.handleClick} component={Link} to="">Controller</MenuItem>
                <MenuItem onClick={this.handleClick} component={Link} to="config">Config</MenuItem>
                <MenuItem onClick={this.handleClick} component={Link} to="debug">Debug</MenuItem>
              </Menu>
            </div>

            <Typography variant="h6" color="inherit" className={classes.grow} align="center">
              {this.state.realName} {this.state.routeName}
            </Typography>
            <OnlineIndicator socket={this.props.socket} mode="websocket"/>

          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

MenuAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MenuAppBar);
