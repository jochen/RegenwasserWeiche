import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));

/*
import React from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBarComponent from './Navigation/Navigation';

ReactDOM.render(
  <MuiThemeProvider>
    <AppBarComponent />
  </MuiThemeProvider>,
  document.getElementById('root'),
);
*/
