import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import red from '@material-ui/core/colors/red';
import IconOn from '@material-ui/icons/Power';
import IconOff from '@material-ui/icons/PowerOff';
import Tooltip from '@material-ui/core/Tooltip';

const styles = theme => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    icon: {
        margin: theme.spacing.unit * 2,
    },
    iconHover: {
        margin: theme.spacing.unit * 2,
        '&:hover': {
            color: red[800],
        },
    },
});

class OnlineIndicator extends React.Component {
    constructor(props) {
        super(props);
        this.wsHandler = this.wsHandler.bind(this);
        this.state = {
            connected: false,
            tip: null,
        };
        if (this.props.mode === "websocket") {
            this.state.tip = {
                Connected: "Websocket Connected",
                Disconnected: "Websocket Disconnected",
            };
        }
        if (this.props.mode === "message") {
            this.state.tip = {
                Connected: "Server Connected",
                Disconnected: "Server Disconnected",
            };
        }
    }

    wsHandler(ev) {
        //console.log(ev);
        try {
            switch (this.props.mode) {
                case "websocket":
                    switch (ev.type) {
                        case "open":
                            //console.log(decodeddata.value);
                            this.setState({
                                connected: true
                            })
                            break;
                        case "close":
                            //console.log(decodeddata.value);
                            this.setState({
                                connected: false
                            })
                            break;
                        default:
                            break;

                    }
                    break;
                case "message":
                    switch (ev.type) {
                        case "message":
                            console.log(this.constructor.name, ev);
                            //console.log(ev.data);
                            const decodeddata = JSON.parse(ev.data);
                            switch (decodeddata.type) {
                                case "mqttconnection":
                                    if (decodeddata.value === 1) {
                                        this.setState({
                                            connected: true
                                        })
                                    }
                                    else {
                                        this.setState({
                                            connected: false
                                        })
                                    }

                                    break;
                                default:
                                    break;
                            }

                            break;
                        default:
                    }
                    break;
                default:
                    break;
            }
        } catch (e) {
            console.log(e, ev.data);
        }
    }
    componentDidMount() {
        this.props.socket.addEventListener("open", this.wsHandler);
        this.props.socket.addEventListener("message", this.wsHandler);
        this.props.socket.addEventListener("close", this.wsHandler);
        this.props.socket.addEventListener("error", this.wsHandler);
    }

    componentWillUnmount() {
        this.props.socket.removeEventListener("open", this.wsHandler);
        this.props.socket.removeEventListener("message", this.wsHandler);
        this.props.socket.removeEventListener("close", this.wsHandler);
        this.props.socket.removeEventListener("error", this.wsHandler);
    }

    render() {
        const { classes, color } = this.props;
        const { connected, tip } = this.state;

        return (
            <div className={classes.root}>
                {connected &&
                    <Tooltip disableFocusListener disableTouchListener title={tip.Connected}>
                        <IconOn className={classes.icon} style={{ color: color }} />
                    </Tooltip>}
                {!connected &&
                    <Tooltip disableFocusListener disableTouchListener title={tip.Disconnected}>
                        <IconOff className={classes.icon} style={{ color: color }} />
                    </Tooltip>}
            </div>
        );
    }
}

OnlineIndicator.propTypes = {
    classes: PropTypes.object.isRequired,
    mode: PropTypes.string.isRequired,
    socket: PropTypes.object.isRequired,
};

export default withStyles(styles)(OnlineIndicator);