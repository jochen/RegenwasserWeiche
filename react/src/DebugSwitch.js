import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

const styles = theme => ({
    label: {
        color: 'white',
    },
});

class DebugSwitch extends React.Component {
    constructor(props) {
        super(props);
        this.state.checked = this.getChecked();
    }
    state = {
        checked: false,
    };

    getChecked = () => {
        if (this.props.value == true || this.props.value == 1) {
            return true;
        }
        return false;
    }

    handleChange = event => {
        console.log("switch change", event)
        this.setState({ checked: event.target.checked });
        var value = 0;
        if (typeof this.props.value === "boolean") {
            if (event.target.checked) {
                value = true;
            }
            else {
                value = false;
            }
        }
        else
        {
            if (event.target.checked) {
                value = 1;
            }
            else {
                value = 0;
            }
        }

        this.props.socket.send(JSON.stringify({
            name: this.props.label,
            type: "logic",
            value,
        }
        ));

    };

    render() {
        //const { checked } = this.state;
        const { classes, label, helpertest, value } = this.props;
        this.state.checked = this.getChecked();
        return (
            //<FormGroup row>
            <FormControlLabel
                control={
                    <Switch
                        checked={this.state.checked}
                        onChange={this.handleChange}
                    //value="checked"
                    />
                }
                label={label}
                classes={{
                    label: classes.label,
                }}

            />
            //</FormGroup>
        );
    }
}

//export default SwitchLabels;
DebugSwitch.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DebugSwitch);
