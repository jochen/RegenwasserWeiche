import React from 'react';
//import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Slider } from 'material-ui-slider';
import calcColor from './calcColor';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = {
    root: {
        //display: 'flex',
        height: 300,
    },
    slider: {
        padding: '0px 22px',
    },
};


class PercentVerticalSlider extends React.Component {
    constructor(props) {
        super(props);
        this.wsHandler = this.wsHandler.bind(this);
        this.state = {
            value: 50,
            color: new calcColor("#00c4ff", "#ffff00", 0, 100),
            doUpdate: true,
        };

    }

    handleChangeComplete = (value) => {
        this.setState({ value });
        this.setDoUpdate();
        this.props.socket.send(JSON.stringify({
            type: "setPercent",
            value,
        }));
    };
    handleChange = (value) => {
        console.log(value);
        this.setState({ value });
        this.setDoUpdate();
    };

    resetDoUpdate() {
        this.setState({ doUpdate: true });
    }

    setDoUpdate() {
        this.setState({ doUpdate: false });
        if (this.timer) {
            clearTimeout(this.timer);
        }
        this.timer = setTimeout(() => {
            this.resetDoUpdate();
        }, 2000);

    }

    wsHandler(ev) {
        //console.log(ev);
        try {
            switch (ev.type) {
                case "open":
                    //console.log(decodeddata.value);
                    this.props.socket.send(JSON.stringify({
                        type: "getPercent",
                        value: this.state.value,
                    }))
                    break;

                case "message":
                    console.log(this.constructor.name, ev);
                    const decodeddata = JSON.parse(ev.data);
                    switch (decodeddata.type) {
                        case "getPercent":
                            //console.log(decodeddata.value);
                            if (this.state.doUpdate) {
                                this.setState({
                                    value: decodeddata.result
                                });
                            }
                            break;
                        case "percent":
                            //console.log(decodeddata.value);
                            if (this.state.doUpdate) {
                                this.setState({
                                    value: decodeddata.value
                                });
                            }
                            break;
                        case "setPercent":
                            //console.log(decodeddata.value);
                            if (decodeddata.result >= 0 && decodeddata.result <= 100)
                            {
                                this.setState({
                                    value: decodeddata.result
                                });    
                            }
                            break;
                        default:
                            break;

                    }
                    break;
                default:
                    break;

            }
        } catch (e) {
            console.log(e, ev.data);
        }
    }
    componentDidMount() {
        this.props.socket.addEventListener("message", this.wsHandler);
        this.props.socket.addEventListener("open", this.wsHandler);
    }

    componentWillUnmount() {
        clearTimeout(this.timer);
        this.props.socket.removeEventListener("message", this.wsHandler);
        this.props.socket.removeEventListener("open", this.wsHandler);
    }

    render() {
        const { classes } = this.props;
        const { value } = this.state;
        const { color } = this.state;

        return (
            <div className={classes.root}>
                <Slider direction="vertical"
                    //defaultValue={value}
                    value={value}
                    color={color.getColor(value)}
                    onChangeComplete={this.handleChangeComplete}
                    onChange={this.handleChange}
                />
            </div>
        );
    }
}

PercentVerticalSlider.propTypes = {
    classes: PropTypes.object.isRequired,
    socket: PropTypes.object.isRequired,
};

export default withStyles(styles)(PercentVerticalSlider);